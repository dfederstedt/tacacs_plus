FROM ubuntu:20.04 AS builder

LABEL Name=tac_plus
ENV VERSION=202006201038


RUN apt update && \
     apt install -y --no-install-recommends gcc libc6-dev make bzip2 libdigest-md5-perl libnet-ldap-perl libio-socket-ssl-perl libpcre3-dev

#downloads project file from site based on ENV VERSION string. To update check site for latest version.
ADD http://www.pro-bono-publico.de/projects/src/DEVEL.$VERSION.tar.bz2 /tac_plus.tar.bz2
RUN tar -xjf /tac_plus.tar.bz2 && \
    cd /PROJECTS && \
    ./configure --prefix=/tacacs && \
    make && \
    make install

# Build a smaller image.
FROM ubuntu:20.04

LABEL maintainer="Daniel Federstedt <daniel.federstedt@nordlo.com>"
#copies from builder
COPY --from=builder /tacacs /tacacs
COPY tac_sample.cfg /etc/tac_plus/tac_plus.cfg
#copys bash script and makes it executable
COPY docker-entrypoint.sh ./
RUN chmod +x docker-entrypoint.sh

#installs requirements and removes cache after.
RUN apt update && \
    apt install -y libdigest-md5-perl libnet-ldap-perl libpcre3-dev && \
    rm -rf /var/cache/apt/* 

EXPOSE 49

ENTRYPOINT ["./docker-entrypoint.sh"]
<h1> Tacacs+ container </h1>
<p> Docker container based on TACACS+(by Marc Huber). Works with Active directory and ldaps.</p>
<a href="https://hub.docker.com/r/federov/tacacs_plus"> https://hub.docker.com/r/federov/tacacs_plus  </a>

<h2> Run </h2>
<p> <b>Test:</b><br> docker run -d -p 49:49 --name tacacs_plus federov/tacacs_plus:latest  <br>
Starts tacacs+ container with port 49 exposed. key is: tac_plus_key and allows login from admin / admin. Obviously for testing only. </p>

<p><b> Run with custom config file:</b> <br>
docker run --name tac_plus -d -p 49:49 -v "$(pwd)/tac_plus.cfg":/etc/tac_plus/tac_plus.cfg --restart=unless-stopped federov/tacacs_plus:latest         
<br>
Starts with custom config file located in your current directory named tac_plus.cfg. </p>
<p><b> Read logs, logins etc: </b><br>
docker logs tac_plus </p>
<p>
See <a href="https://gitlab.com/dfederstedt/tacacs_plus/-/blob/master/ldaps_sample.cfg">ldaps_sample.cfg at https://gitlab.com/dfederstedt/tacacs_plus </a> for an example on how to configure tacacs+ with ldaps and Microsofts Active Directory instead of cleartext ldap. Requires ldaps configured on target server.
Make sure to chmod 600 the config file, since it will contain password for service account.
</p>
<h2>Troubleshoot:</h2>
docker exec -it tacacs_plus bash
<p> <b>Build from Dockerfile:</b> docker build -t tacacs_plus:latest . </p>
</p>

<h2> Sources: </h2> 
<a href="https://github.com/lfkeitel/docker-tacacs-plus/">https://github.com/lfkeitel/docker-tacacs-plus/ </a>          
  <br>
<a href="https://github.com/dchidell/docker-tacacs/"> https://github.com/dchidell/docker-tacacs/ </a>
              <br>
<a href="https://networkengineering.stackexchange.com/questions/45436/" >https://networkengineering.stackexchange.com/questions/45436/how-do-you-configure-a-tacacs-tac-plus-server-on-ubuntu-16-04-that-authenticate </a>
<br>
<br>
<p> Based on TACACS+ by Marc Huber.
<br>
<a href="http://www.pro-bono-publico.de/projects/tac_plus.html">
http://www.pro-bono-publico.de/projects/tac_plus.html </a>          <br>
This product includes software developed by Marc Huber         <br>
     (Marc.Huber@web.de).  <br>
<a href="http://www.pro-bono-publico.de/projects/unpacked/LICENSE"> 
http://www.pro-bono-publico.de/projects/unpacked/LICENSE </a>
</p>